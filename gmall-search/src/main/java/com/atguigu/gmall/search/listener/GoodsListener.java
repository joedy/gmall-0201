package com.atguigu.gmall.search.listener;

import com.atguigu.gmall.common.bean.ResponseVo;
import com.atguigu.gmall.pms.entity.*;
import com.atguigu.gmall.search.feign.GmallPmsClient;
import com.atguigu.gmall.search.feign.GmallWmsClient;
import com.atguigu.gmall.search.pojo.Goods;
import com.atguigu.gmall.search.pojo.SearchAttrValue;
import com.atguigu.gmall.wms.entity.WareSkuEntity;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class GoodsListener {

    @Autowired
    private GmallPmsClient pmsClient;

    @Autowired
    private GmallWmsClient wmsClient;

    @Autowired
    private ElasticsearchRestTemplate restTemplate;

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue("SEARCH.INSERT.QUEUE"),
            exchange = @Exchange(value = "PMS.SPU.EXCHANGE", type = ExchangeTypes.TOPIC, ignoreDeclarationExceptions = "true"),
            key = {"item.insert"}
    ))
    public void syncData(Long spuId, Channel channel, Message message) throws IOException {
        if (spuId == null) {
            channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
            return;
        }

        // 根据spuId查询spu
        ResponseVo<SpuEntity> spuEntityResponseVo = this.pmsClient.querySpuById(spuId);
        SpuEntity spu = spuEntityResponseVo.getData();
        if (spu == null){
            channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
            return;
        }

        ResponseVo<List<SkuEntity>> skuResponseVo = this.pmsClient.querySkusBySpuId(spu.getId());
        List<SkuEntity> skus = skuResponseVo.getData();
        // 如果当前spu下的sku为空，则直接遍历下个spu
        if (CollectionUtils.isEmpty(skus)){
            return; // 在增强版的forEach中，return不是结束，类似于continue。
        }

        // 同一个spu的sku品牌肯定是一样的：根据品牌id查询品牌
        ResponseVo<BrandEntity> brandEntityResponseVo = this.pmsClient.queryBrandById(spu.getBrandId());
        BrandEntity brandEntity = brandEntityResponseVo.getData();

        // 同一个spu的sku分类肯定是一样的：根据分类id查询分类
        ResponseVo<CategoryEntity> categoryEntityResponseVo = this.pmsClient.queryCategoryById(spu.getCategoryId());
        CategoryEntity categoryEntity = categoryEntityResponseVo.getData();

        // 同一个spu基本类型的检索属性和值，肯定是一样的：根据分类id和spuId查询检索类型的基本属性和值
        ResponseVo<List<SpuAttrValueEntity>> baseAttrResponseVo = this.pmsClient.querySearchAttrValuesByCidAndSpuId(spu.getCategoryId(), spu.getId());
        List<SpuAttrValueEntity> spuAttrValueEntities = baseAttrResponseVo.getData();

        // 根据spuId查询spu下skus
        this.restTemplate.save(skus.stream().map(sku -> {
            Goods goods = new Goods();
            // 设置sku的基本信息
            goods.setSkuId(sku.getId());
            goods.setTitle(sku.getTitle());
            goods.setSubtitle(sku.getSubtitle());
            goods.setDefaultImage(sku.getDefaultImage());
            goods.setPrice(sku.getPrice().doubleValue());

            // 设置spu的创建时间
            goods.setCreateTime(spu.getCreateTime());

            // 根据skuId查询库存，获取到销量及库存信息
            ResponseVo<List<WareSkuEntity>> wareSkuResponseVo = this.wmsClient.queryWareSkusBySkuId(sku.getId());
            List<WareSkuEntity> wareSkuEntities = wareSkuResponseVo.getData();
            if (!CollectionUtils.isEmpty(wareSkuEntities)){
                // 销量
                goods.setSales(wareSkuEntities.stream().map(WareSkuEntity::getSales).reduce((a, b) -> a + b).get());
                // 是否有货
                goods.setStore(wareSkuEntities.stream().anyMatch(wareSkuEntity -> wareSkuEntity.getStock() - wareSkuEntity.getStockLocked() > 0));
            }

            // 设置品牌参数
            if (brandEntity != null) {
                goods.setBrandId(brandEntity.getId());
                goods.setBrandName(brandEntity.getName());
                goods.setLogo(brandEntity.getLogo());
            }

            // 设置分类参数
            if (categoryEntity != null) {
                goods.setCategoryId(categoryEntity.getId());
                goods.setCategoryName(categoryEntity.getName());
            }

            // 设置检索类型的规格参数和值
            List<SearchAttrValue> searchAttrs = new ArrayList<>();
            // pms_sku_attr_value：当前sku的检索类型的销售属性和值
            ResponseVo<List<SkuAttrValueEntity>> saleAttrResponseVo = this.pmsClient.querySearchAttrValuesByCidAndSkuId(sku.getCategoryId(), sku.getId());
            List<SkuAttrValueEntity> skuAttrValueEntities = saleAttrResponseVo.getData();
            if (!CollectionUtils.isEmpty(skuAttrValueEntities)){
                // 把entity集合转化成SearchAttrValue集合，然后放入searchAttrs集合
                searchAttrs.addAll(skuAttrValueEntities.stream().map(skuAttrValueEntity -> {
                    SearchAttrValue searchAttrValue = new SearchAttrValue();
                    BeanUtils.copyProperties(skuAttrValueEntity, searchAttrValue);
                    return searchAttrValue;
                }).collect(Collectors.toList()));
            }

            // pms_spu_attr_value：当前spu的检索类型的基本属性和值
            if (!CollectionUtils.isEmpty(spuAttrValueEntities)){
                // 把entity集合转化成SearchAttrValue集合，并放入searchAttrs集合
                searchAttrs.addAll(spuAttrValueEntities.stream().map(spuAttrValueEntity -> {
                    SearchAttrValue searchAttrValue = new SearchAttrValue();
                    BeanUtils.copyProperties(spuAttrValueEntity, searchAttrValue);
                    return searchAttrValue;
                }).collect(Collectors.toList()));
            }

            goods.setSearchAttrs(searchAttrs);
            return goods;
        }).collect(Collectors.toList()));

        channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
    }
}
