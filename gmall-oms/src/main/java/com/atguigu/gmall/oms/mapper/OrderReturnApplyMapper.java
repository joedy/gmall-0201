package com.atguigu.gmall.oms.mapper;

import com.atguigu.gmall.ums.entity.OrderReturnApplyEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单退货申请
 * 
 * @author fengge
 * @email fengge@atguigu.com
 * @date 2023-08-01 10:29:17
 */
@Mapper
public interface OrderReturnApplyMapper extends BaseMapper<OrderReturnApplyEntity> {
	
}
