package com.atguigu.gmall.oms.mapper;

import com.atguigu.gmall.ums.entity.OrderSettingEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单配置信息
 * 
 * @author fengge
 * @email fengge@atguigu.com
 * @date 2023-08-01 10:29:17
 */
@Mapper
public interface OrderSettingMapper extends BaseMapper<OrderSettingEntity> {
	
}
