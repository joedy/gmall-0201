package com.atguigu.gmall.payment.feign;

import com.atguigu.gmall.ums.api.GmallOmsApi;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient("oms-service")
public interface GmallOmsClient extends GmallOmsApi {
}
