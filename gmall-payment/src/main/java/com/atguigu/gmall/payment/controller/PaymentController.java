package com.atguigu.gmall.payment.controller;

import com.alibaba.fastjson.JSON;
import com.alipay.api.AlipayApiException;
import com.atguigu.gmall.common.exception.OrderException;
import com.atguigu.gmall.payment.config.AlipayTemplate;
import com.atguigu.gmall.payment.interceptor.LoginInterceptor;
import com.atguigu.gmall.payment.pojo.PaymentInfoEntity;
import com.atguigu.gmall.payment.service.PaymentService;
import com.atguigu.gmall.payment.vo.PayAsyncVo;
import com.atguigu.gmall.payment.vo.PayVo;
import com.atguigu.gmall.payment.vo.UserInfo;
import com.atguigu.gmall.ums.entity.OrderEntity;
import org.apache.commons.lang3.StringUtils;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.util.Date;

@Controller
public class PaymentController {

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private AlipayTemplate alipayTemplate;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @GetMapping("pay.html")
    public String paySelect(@RequestParam("orderToken")String orderToken, Model model){
        OrderEntity orderEntity = this.paymentService.queryOrderByToken(orderToken);

        // 1.校验订单是否为null
        if (orderEntity == null) {
            throw new OrderException("您要支付的订单为空");
        }

        // 2.校验订单状态是否为待支付状态
        if (orderEntity.getStatus() != 0) {
            throw new OrderException("当前订单不可支付！");
        }

        // 3.校验订单所属用户是否是当前用户
        UserInfo userInfo = LoginInterceptor.getUserInfo();
        Long userId = userInfo.getUserId();
        if (orderEntity.getUserId() != userId) {
            throw new OrderException("当前订单不属于您！");
        }

        model.addAttribute("orderEntity", orderEntity);

        return "pay";
    }

    @GetMapping("alipay.html")
    @ResponseBody // 以其他视图形式展示方法的返回结果集
    public Object alipay(@RequestParam("orderToken")String orderToken) throws AlipayApiException {

        OrderEntity orderEntity = this.paymentService.queryOrderByToken(orderToken);
        // 1.校验订单是否为null
        if (orderEntity == null) {
            throw new OrderException("您要支付的订单为空");
        }

        // 2.校验订单状态是否为待支付状态
        if (orderEntity.getStatus() != 0) {
            throw new OrderException("当前订单不可支付！");
        }

        // 3.校验订单所属用户是否是当前用户
        UserInfo userInfo = LoginInterceptor.getUserInfo();
        Long userId = userInfo.getUserId();
        if (orderEntity.getUserId() != userId) {
            throw new OrderException("当前订单不属于您！");
        }

        // TODO: 调用支付宝支付接口，打开支付页面
        PayVo payVo = new PayVo();
        payVo.setOut_trade_no(orderEntity.getOrderSn());
        // 不要这么写，一定要写0.01
        payVo.setTotal_amount("0.01");
        payVo.setSubject("谷粒商城支付平台");
        Long payId = this.paymentService.savePayment(payVo);
        payVo.setPassback_params(payId.toString());
        return this.alipayTemplate.pay(payVo);
    }

    @GetMapping("pay/success")
    public String paysuccess(){

        return "paysuccess";
    }

    @PostMapping("pay/ok")
    @ResponseBody
    public String payOk(PayAsyncVo asyncVo){

        // 1.验签：确保请求是支付宝发送的
        Boolean flag = this.alipayTemplate.checkSignature(asyncVo);
        if (!flag){
            return "failure";
        }

        // 2.校验业务参数是否和请求中的一致：app_id、out_trade_no、total_amount
        // 异步通知中的业务参数
        String appId = asyncVo.getApp_id();
        String outTradeNo = asyncVo.getOut_trade_no();
        String totalAmount = asyncVo.getTotal_amount();
        String payId = asyncVo.getPassback_params();
        PaymentInfoEntity paymentInfoEntity = this.paymentService.queryPaymentInfo(payId);
        if (!StringUtils.equals(appId, this.alipayTemplate.getApp_id())
                || !StringUtils.equals(outTradeNo, paymentInfoEntity.getOutTradeNo())
                || new BigDecimal(totalAmount).compareTo(paymentInfoEntity.getTotalAmount()) != 0){
            return "failure";
        }

        // 3.校验支付状态：trade_status   == TRADE_SUCCESS
        String tradeStatus = asyncVo.getTrade_status();
        if (!"TRADE_SUCCESS".equals(tradeStatus)){
            return "failure";
        }

        // 4.修改对账记录为已支付
        paymentInfoEntity.setTradeNo(asyncVo.getTrade_no());
        paymentInfoEntity.setPaymentStatus(1);
        paymentInfoEntity.setCallbackTime(new Date());
        paymentInfoEntity.setCallbackContent(JSON.toJSONString(asyncVo));
        if (this.paymentService.updatePaymentInfo(paymentInfoEntity) == 1) {
            // 5.发送消息给oms修改订单状态
            this.rabbitTemplate.convertAndSend("ORDER.EXCHANGE", "order.pay", outTradeNo);
        }

        // 6.响应成功
        return "success";
    }
}
