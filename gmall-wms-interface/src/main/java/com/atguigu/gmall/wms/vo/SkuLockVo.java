package com.atguigu.gmall.wms.vo;

import lombok.Data;

@Data
public class SkuLockVo {

    // 请求参数
    private Long skuId;
    private Integer count;

    private Boolean lock; // 锁定状态：库存充足-true 库存不充足-false
    private Long wareSkuId; // 如果锁定成功，需要记录锁定库存的id
}
