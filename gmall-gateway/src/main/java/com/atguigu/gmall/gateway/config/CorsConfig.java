package com.atguigu.gmall.gateway.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;

@Configuration
public class CorsConfig {

    @Bean
    public CorsWebFilter corsWebFilter(){

        // 初始化cors规则配置类
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        // 允许跨域访问的域名，*-代表允许所有域名，但是不要写*。1-不安全 2-不能携带cookie
        corsConfiguration.addAllowedOrigin("http://manager.gmall.com");
        corsConfiguration.addAllowedOrigin("http://localhost:1000");
        corsConfiguration.addAllowedOrigin("http://gmall.com");
        corsConfiguration.addAllowedOrigin("http://www.gmall.com");
        // 允许所有方法跨域访问
        corsConfiguration.addAllowedMethod("*");
        // 允许携带任何头信息跨域访问
        corsConfiguration.addAllowedHeader("*");
        // 允许携带cookie
        corsConfiguration.setAllowCredentials(true);

        // 初始化基于url规则的配置源对象
        UrlBasedCorsConfigurationSource configurationSource = new UrlBasedCorsConfigurationSource();
        configurationSource.registerCorsConfiguration("/**", corsConfiguration);
        // 初始化cors过滤器
        return new CorsWebFilter(configurationSource);
    }
}
