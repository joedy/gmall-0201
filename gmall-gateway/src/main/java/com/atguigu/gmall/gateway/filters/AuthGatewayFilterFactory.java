package com.atguigu.gmall.gateway.filters;

import com.atguigu.gmall.common.utils.CookieUtils;
import com.atguigu.gmall.common.utils.IpUtils;
import com.atguigu.gmall.common.utils.JwtUtils;
import com.atguigu.gmall.gateway.config.JwtProperties;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.http.HttpCookie;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.MultiValueMap;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Component
@EnableConfigurationProperties(JwtProperties.class)
public class AuthGatewayFilterFactory extends AbstractGatewayFilterFactory<AuthGatewayFilterFactory.PathConfig> {

    @Autowired
    private JwtProperties properties;

    public AuthGatewayFilterFactory() {
        super(PathConfig.class);
    }

    @Override
    public List<String> shortcutFieldOrder() {
        return Arrays.asList("paths");
    }

    @Override
    public ShortcutType shortcutType() {
        return ShortcutType.GATHER_LIST;
    }

    @Override
    public GatewayFilter apply(PathConfig config) {

        return (exchange, chain) -> {

            // ServerHttpRequest等价于 HttpServletRequest
            ServerHttpRequest request = exchange.getRequest();
            ServerHttpResponse response = exchange.getResponse();

            // 1.判断当前请求在不在拦截名单之中（黑名单），不在则直接放行
            String curPath = request.getURI().getPath(); // 获取当前请求的路径
            List<String> paths = config.paths; // 拦截名单
            // 如果拦截名单不为空并且当前请求路径不以拦截名单中的任一路径开头则直接放行
            if (!CollectionUtils.isEmpty(paths) && !paths.stream().anyMatch(path -> curPath.startsWith(path))){
                return chain.filter(exchange);
            }

            // 2.获取token（jwt），同步请求-cookie 异步-头信息
            String token = request.getHeaders().getFirst(properties.getToken());
            if (StringUtils.isBlank(token)){
                MultiValueMap<String, HttpCookie> cookies = request.getCookies();
                // 如果cookie不为空，并且包含GMALL-TOKEN时，才需要获取cookie
                if (!CollectionUtils.isEmpty(cookies) && cookies.containsKey(properties.getCookieName())){
                    HttpCookie cookie = cookies.getFirst(properties.getCookieName());
                    token = cookie.getValue();
                }
            }

            // 3.判断token是否为空，如果为空说明没有登录，则请求结束并重定向到登录页面
            if (StringUtils.isBlank(token)){
                response.setStatusCode(HttpStatus.SEE_OTHER);
                response.getHeaders().set("Location", "http://sso.gmall.com/toLogin.html?returnUrl=" + request.getURI());
                // 拦截
                return response.setComplete();
            }

            try {
                // 4.解析token，如果出现异常说明token不合法，则请求结束并重定向到登录页面
                Map<String, Object> map = JwtUtils.getInfoFromToken(token, properties.getPublicKey());

                // 5.判断当前请求的ip地址 和 登录用户载荷中的ip 是否一致，如果不一致说明token不属于当前用户，则请求结束并重定向到登录页面
                String curIp = IpUtils.getIpAddressAtGateway(request); // 当前请求的ip地址
                String ip = map.get("ip").toString();  // 登录用户的ip地址
                if (!StringUtils.equals(curIp, ip)){
                    response.setStatusCode(HttpStatus.SEE_OTHER);
                    response.getHeaders().set("Location", "http://sso.gmall.com/toLogin.html?returnUrl=" + request.getURI());
                    // 拦截
                    return response.setComplete();
                }

                // 6.把解析后的载荷内容传递给后续服务（解析比较耗时）
                request.mutate().header("userId", map.get("userId").toString())
                        .header("username", map.get("username").toString()).build();
                exchange.mutate().request(request).build();

                // 7.放行
                return chain.filter(exchange);
            } catch (Exception e) {
                response.setStatusCode(HttpStatus.SEE_OTHER);
                response.getHeaders().set("Location", "http://sso.gmall.com/toLogin.html?returnUrl=" + request.getURI());
                // 拦截
                return response.setComplete();
            }
        };
    }

    @Data
    public static class PathConfig {
        private List<String> paths;
    }
}
