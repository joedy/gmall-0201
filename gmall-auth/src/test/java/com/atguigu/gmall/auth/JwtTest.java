package com.atguigu.gmall.auth;

import com.atguigu.gmall.common.utils.JwtUtils;
import com.atguigu.gmall.common.utils.RsaUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.HashMap;
import java.util.Map;

public class JwtTest {

    // 别忘了创建D:\\project\rsa目录
	private static final String pubKeyPath = "D:\\project-0201\\rsa\\rsa.pub";
    private static final String priKeyPath = "D:\\project-0201\\rsa\\rsa.pri";

    private PublicKey publicKey;

    private PrivateKey privateKey;

    @Test
    public void testRsa() throws Exception {
        RsaUtils.generateKey(pubKeyPath, priKeyPath, "sdfdsDFDFs21321@#kfjds");
    }

    @BeforeEach
    public void testGetRsa() throws Exception {
        this.publicKey = RsaUtils.getPublicKey(pubKeyPath);
        this.privateKey = RsaUtils.getPrivateKey(priKeyPath);
    }

    @Test
    public void testGenerateToken() throws Exception {
        Map<String, Object> map = new HashMap<>();
        map.put("id", "11");
        map.put("username", "liuyan");
        // 生成token
        String token = JwtUtils.generateToken(map, privateKey, 2);
        System.out.println("token = " + token);
    }

    @Test
    public void testParseToken() throws Exception {
        String token = "eyJhbGciOiJSUzI1NiJ9.eyJpZCI6IjExIiwidXNlcm5hbWUiOiJsaXV5YW4iLCJleHAiOjE2OTAyNzM3MTd9.KRlfcRyjnTlntzhn8t7Q_5prL_iSrsfBm4QBEYQJln2M_gquYWAN41uq8QjNwA6EXvZeH-EPDmFzsQbp-PHKQ6vCaasRy4vGne1wOiDsDbkPyoXoxaFT9qph1kHM5wU1lSn8Y-UR0rZK_s06xq8w3sWV0sx30pXm7i7YygOpowZA-zjG3YlgZCVU5p8E1RF3NTkQNY8F6WhwISVRtsUZX3GASzV7cA6pSByC6ukwnd76bBHfjUeFafRwzuqY6Iwi8crf9Kc_C1s7I09dz6sVH-P2me4lxCtN-RKKjAfJZNOq7DWlIJWA-NdDE2ntyEVzep1vMkkG5kwp-F-fTMH-MA";

        // 解析token
        Map<String, Object> map = JwtUtils.getInfoFromToken(token, publicKey);
        System.out.println("id: " + map.get("id"));
        System.out.println("userName: " + map.get("username"));
    }
}