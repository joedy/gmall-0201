package com.atguigu.gmall.ums.mapper;

import com.atguigu.gmall.ums.entity.UserLevelEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员等级表
 * 
 * @author fengge
 * @email fengge@atguigu.com
 * @date 2023-07-25 10:45:52
 */
@Mapper
public interface UserLevelMapper extends BaseMapper<UserLevelEntity> {
	
}
