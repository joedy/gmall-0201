package com.atguigu.gmall.cart.controller;

import com.atguigu.gmall.cart.interceptor.LoginInterceptor;
import com.atguigu.gmall.cart.pojo.Cart;
import com.atguigu.gmall.cart.service.CartService;
import com.atguigu.gmall.common.bean.ResponseVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@Controller
public class CartController {

    @Autowired
    private CartService cartService;

    @GetMapping
    public String addCart(Cart cart){
        this.cartService.addCart(cart);
        return "redirect:http://cart.gmall.com/addCart.html?skuId=" + cart.getSkuId() + "&count=" + cart.getCount();
    }

    @GetMapping("addCart.html")
    public String queryCart(Cart cart, Model model){
        // 覆盖cart对象之前，先获取本次新增的数量
        BigDecimal count = cart.getCount();
        // 用数据库中的cart对象，覆盖掉当前cart对象
        cart = this.cartService.queryCartBySkuId(cart.getSkuId());
        cart.setCount(count);
        model.addAttribute("cart", cart);
        return "addCart";
    }

    @GetMapping("cart.html")
    public String queryCarts(Model model){

        List<Cart> carts = this.cartService.queryCarts();
        model.addAttribute("carts", carts);

        return "cart";
    }

    @PostMapping("updateNum")
    @ResponseBody
    public ResponseVo updateNum(@RequestBody Cart cart){
        this.cartService.updateNum(cart);
        return ResponseVo.ok();
    }

    @PostMapping("deleteCart")
    @ResponseBody
    public ResponseVo deleteCart(@RequestParam("skuId")Long skuId){
        this.cartService.deleteCart(skuId);
        return ResponseVo.ok();
    }

    @GetMapping("user/{userId}")
    @ResponseBody
    public ResponseVo<List<Cart>> queryCheckedCartsByUserId(@PathVariable("userId")Long userId){
        List<Cart> carts = this.cartService.queryCheckedCartsByUserId(userId);
        return ResponseVo.ok(carts);
    }

    @ResponseBody
    @GetMapping("test")
    public String test(HttpServletRequest request){
        long now = System.currentTimeMillis();
        System.out.println("controller方法开始执行。。。。。");
        this.cartService.execute1();
        this.cartService.execute2();
//        future1.addCallback(result -> {
//            System.out.println("future1方法正常执行，返回：" + result);
//        }, ex -> {
//            System.out.println("future1异常执行，抛出异常信息：" + ex.getMessage());
//        });
//        future2.addCallback(result -> {
//            System.out.println("future2方法正常执行，返回：" + result);
//        }, ex -> {
//            System.out.println("future2异常执行，抛出异常信息：" + ex.getMessage());
//        });
        System.out.println("controller方法执行完成：" + (System.currentTimeMillis() - now));
        return "hello cart";
    }
}
