package com.atguigu.gmall.cart.interceptor;

import com.atguigu.gmall.cart.config.JwtProperties;
import com.atguigu.gmall.cart.pojo.UserInfo;
import com.atguigu.gmall.common.utils.CookieUtils;
import com.atguigu.gmall.common.utils.JwtUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.UUID;

//@Scope("prototype")
@Component
public class LoginInterceptor implements HandlerInterceptor {

//    public UserInfo userInfo;
    private static final ThreadLocal<UserInfo> THREAD_LOCAL = new ThreadLocal<>();

    @Autowired
    private JwtProperties properties;

    /**
     * 在controller方法执行之前执行
     * @param request current HTTP request
     * @param response current HTTP response
     * @param handler chosen handler to execute, for type and/or instance evaluation
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        System.out.println("前值方法：在controller方法执行之前执行。。。。。");

        // 获取登录信息
        String userKey = CookieUtils.getCookieValue(request, this.properties.getUserKey());
        String token = CookieUtils.getCookieValue(request, this.properties.getCookieName());
        // 不管有没有登录都需要userKey，如果userkey为空则生成一个放入cookie
        if (StringUtils.isBlank(userKey)){
            userKey = UUID.randomUUID().toString();
            CookieUtils.setCookie(request, response, properties.getUserKey(), userKey, this.properties.getExpire());
        }
        // 解析token获取userId
        Long userId = null;
        if (StringUtils.isNotBlank(token)){
            Map<String, Object> map = JwtUtils.getInfoFromToken(token, this.properties.getPublicKey());
            userId = Long.valueOf(map.get("userId").toString());
        }

        //userInfo = new UserInfo(4L, UUID.randomUUID().toString());
//        request.setAttribute("userId", 4L);
//        request.setAttribute("userKey", UUID.randomUUID().toString());
        THREAD_LOCAL.set(new UserInfo(userId, userKey));

        // 如果为true则放行，否则被拦截
        return true;
    }

    public static UserInfo getUserInfo(){
        return THREAD_LOCAL.get();
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        System.out.println("后置方法：在controller方法执行之后执行。。。。。");
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        System.out.println("完成方法：在视图渲染完成之后执行。通常释放资源");
        // 由于使用的是tomcat线程池，所以此处必须手动remove，否则会引发内存泄漏，最终导致OOM宕机
        THREAD_LOCAL.remove();
    }
}
