package com.atguigu.gmall.cart.service;

import com.alibaba.fastjson.JSON;
import com.atguigu.gmall.cart.feign.GmallPmsClient;
import com.atguigu.gmall.cart.feign.GmallSmsClient;
import com.atguigu.gmall.cart.feign.GmallWmsClient;
import com.atguigu.gmall.cart.interceptor.LoginInterceptor;
import com.atguigu.gmall.cart.mapper.CartMapper;
import com.atguigu.gmall.cart.pojo.Cart;
import com.atguigu.gmall.cart.pojo.UserInfo;
import com.atguigu.gmall.common.bean.ResponseVo;
import com.atguigu.gmall.common.exception.CartException;
import com.atguigu.gmall.pms.entity.SkuAttrValueEntity;
import com.atguigu.gmall.pms.entity.SkuEntity;
import com.atguigu.gmall.sms.vo.ItemSaleVo;
import com.atguigu.gmall.wms.entity.WareSkuEntity;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.concurrent.ListenableFuture;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
public class CartService {

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private CartAsyncService asyncService;

    @Autowired
    private GmallPmsClient pmsClient;

    @Autowired
    private GmallWmsClient wmsClient;

    @Autowired
    private GmallSmsClient smsClient;

    private static final String KEY_PREFIX = "cart:info:";
    private static final String PRICE_PREFIX = "cart:price:";

    /**
     * 新增购物车
     * @param cart
     */
    public void addCart(Cart cart) {

        // 1.获取登录状态：登录-userId 未登录-userKey
        String userId = getUserId();

        // 2.判断当前用户的购物车是否包含该商品
        // 获取当前用户的购物车：Map<skuId, CartJson>
        BoundHashOperations<String, Object, Object> hashOps = this.redisTemplate.boundHashOps(KEY_PREFIX + userId);
        String skuId = cart.getSkuId().toString();
        BigDecimal count = cart.getCount(); // 本次新增数量
        if (hashOps.hasKey(cart.getSkuId().toString())) {
            // 包含-累加数量
            String json = hashOps.get(skuId).toString();
            cart = JSON.parseObject(json, Cart.class);
            cart.setCount(cart.getCount().add(count));
            // 写入数据库
            this.asyncService.updateCart(userId, skuId, cart);
        } else {
            // 不包含-新增记录
            cart.setUserId(userId);
            cart.setCheck(true);
            // 根据skuId查询sku
            ResponseVo<SkuEntity> skuEntityResponseVo = this.pmsClient.querySkuById(cart.getSkuId());
            SkuEntity skuEntity = skuEntityResponseVo.getData();
            if (skuEntity == null) {
                throw new CartException("您加入购物车的商品不存在！");
            }
            cart.setTitle(skuEntity.getTitle());
            cart.setPrice(skuEntity.getPrice());
            cart.setDefaultImage(skuEntity.getDefaultImage());

            // 是否有货
            ResponseVo<List<WareSkuEntity>> wareResponseVo = this.wmsClient.queryWareSkusBySkuId(cart.getSkuId());
            List<WareSkuEntity> wareSkuEntities = wareResponseVo.getData();
            if (!CollectionUtils.isEmpty(wareSkuEntities)){
                cart.setStore(wareSkuEntities.stream().anyMatch(wareSkuEntity -> wareSkuEntity.getStock() - wareSkuEntity.getStockLocked() > 0));
            }


            // 销售属性
            ResponseVo<List<SkuAttrValueEntity>> saleAttrResponseVo = this.pmsClient.querySaleAttrValueBySkuId(cart.getSkuId());
            List<SkuAttrValueEntity> skuAttrValueEntities = saleAttrResponseVo.getData();
            cart.setSaleAttrs(JSON.toJSONString(skuAttrValueEntities));

            // 营销信息
            ResponseVo<List<ItemSaleVo>> salesResponseVo = this.smsClient.querySalesBySkuId(cart.getSkuId());
            List<ItemSaleVo> itemSaleVos = salesResponseVo.getData();
            cart.setSales(JSON.toJSONString(itemSaleVos));

            // 保存到数据库
            this.asyncService.insertCart(userId, cart);
            // 添加实时价格缓存
            this.redisTemplate.opsForValue().setIfAbsent(PRICE_PREFIX + skuId, skuEntity.getPrice().toString());
        }
        hashOps.put(skuId, JSON.toJSONString(cart));
    }


    private String getUserId() {
        UserInfo userInfo = LoginInterceptor.getUserInfo();
        // 如果userId不为空，取userId，否则取userKey
        String userId = userInfo.getUserKey();
        if (userInfo.getUserId() != null){
            userId = userInfo.getUserId().toString();
        }
        return userId;
    }

    /**
     * 新增后的回显
     * @param skuId
     * @return
     */
    public Cart queryCartBySkuId(Long skuId) {

        // 获取登录状态
        String userId = this.getUserId();

        // 获取内层Map<SkuId, CartJson>
        BoundHashOperations<String, Object, Object> hashOps = this.redisTemplate.boundHashOps(KEY_PREFIX + userId);
        if (hashOps.hasKey(skuId.toString())){
            String json = hashOps.get(skuId.toString()).toString();
            return JSON.parseObject(json, Cart.class);
        }
        throw new CartException("您的购物车中不包含该商品！");
    }

    @Async
    public void execute1() {
        try {
            System.out.println("execute1方法开始执行。。。");
            TimeUnit.SECONDS.sleep(4);
            int i = 1/0;
            System.out.println("execute1方法结束。。。");
        } catch (InterruptedException e) {
//            return AsyncResult.forExecutionException(e);
            e.printStackTrace();
        }
//        return AsyncResult.forValue("hello execute1");
    }

    @Async
    public void execute2() {
        try {
            System.out.println("execute2方法开始执行。。。");
            TimeUnit.SECONDS.sleep(5);
            System.out.println("execute2方法结束。。。");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
//        return AsyncResult.forValue("hello execute2");
    }

    public List<Cart> queryCarts() {

        UserInfo userInfo = LoginInterceptor.getUserInfo();
        // 1.根据userKey查询未登录的购物车 Map<skuId, CartJson>
        String userKey = userInfo.getUserKey();
        BoundHashOperations<String, Object, Object> unloginHashOps = this.redisTemplate.boundHashOps(KEY_PREFIX + userKey);
        List<Object> unloginCartJsons = unloginHashOps.values(); // List<CartJson> --> List<Cart>
        List<Cart> unloginCarts = null;
        if (!CollectionUtils.isEmpty(unloginCartJsons)){
            unloginCarts = unloginCartJsons.stream().map(cartJson -> {
                Cart cart = JSON.parseObject(cartJson.toString(), Cart.class);
                cart.setCurrentPrice(new BigDecimal(this.redisTemplate.opsForValue().get(PRICE_PREFIX + cart.getSkuId())));
                return cart;
            }).collect(Collectors.toList());
        }

        // 2.获取UserId，判断登录状态，如果未登录直接返回未登录的购物车
        Long userId = userInfo.getUserId();
        if (userId == null) {
            return unloginCarts;
        }

        // 3.根据userId，获取已登录的购物车，把未登录的购物车合并到已登录的购物车中去 Map<skuId, CartJson>
        BoundHashOperations<String, Object, Object> loginHashOps = this.redisTemplate.boundHashOps(KEY_PREFIX + userId);
        // 遍历未登录的购物车合并到已登录的购物车中去
        if (!CollectionUtils.isEmpty(unloginCarts)){
            unloginCarts.forEach(cart -> { // 未登录购物车对象
                String skuId = cart.getSkuId().toString(); // 未登录购物车的skuId
                BigDecimal count = cart.getCount(); // 未登录购物车中的count
                if (loginHashOps.hasKey(skuId)) {
                    // 如果已登录的购物车中已经包含了该记录--> 数量累加
                    String json = loginHashOps.get(skuId).toString();
                    cart = JSON.parseObject(json, Cart.class);  // 已登录购物车对象
                    cart.setCount(cart.getCount().add(count));
                    this.asyncService.updateCart(userId.toString(), skuId, cart);
                } else {
                    // 如果已登录的购物车中不包含该记录则新增记录
                    cart.setId(null);
                    cart.setUserId(userId.toString());
                    this.asyncService.insertCart(userId.toString(), cart);
                }
                loginHashOps.put(skuId, JSON.toJSONString(cart));
            });

            // 4.清空未登录的购物车
            this.asyncService.deleteCartByUserId(userKey);
            this.redisTemplate.delete(KEY_PREFIX + userKey);
        }

        // 5.返回合并后的已登录的购物车
        List<Object> loginCartJsons = loginHashOps.values();
        if (!CollectionUtils.isEmpty(loginCartJsons)){
            return loginCartJsons.stream().map(json -> {
                Cart cart = JSON.parseObject(json.toString(), Cart.class);
                cart.setCurrentPrice(new BigDecimal(this.redisTemplate.opsForValue().get(PRICE_PREFIX + cart.getSkuId())));
                return cart;
            }).collect(Collectors.toList());
        }
        return null;
    }

    public void updateNum(Cart cart) {
        // 1.获取登录状态
        String userId = this.getUserId();

        // 2.获取购物车
        BoundHashOperations<String, Object, Object> hashOps = this.redisTemplate.boundHashOps(KEY_PREFIX + userId);
        String cartJson = hashOps.get(cart.getSkuId().toString()).toString();
        // 获取要更新的数据量
        BigDecimal count = cart.getCount();
        cart = JSON.parseObject(cartJson, Cart.class);
        cart.setCount(count);
        // 写入数据库
        this.asyncService.updateCart(userId, cart.getSkuId().toString(), cart);
        hashOps.put(cart.getSkuId().toString(), JSON.toJSONString(cart));
    }

    public void deleteCart(Long skuId) {
        String userId = this.getUserId();

        BoundHashOperations<String, Object, Object> hashOps = this.redisTemplate.boundHashOps(KEY_PREFIX + userId);
        hashOps.delete(skuId.toString());
        this.asyncService.deleteCartByUserIdAndSkuId(userId, skuId);
    }

    public List<Cart> queryCheckedCartsByUserId(Long userId) {

        // TODO: 1.根据userKey查询未登录的购物车；2.取消已登录状态的购物车中所有商品的选中状态 3.把未登录的购物车合并到已登录的购物车中去

        // 4.根据userId查询已选中额购物车
        BoundHashOperations<String, Object, Object> hashOps = this.redisTemplate.boundHashOps(KEY_PREFIX + userId);
        List<Object> cartJsons = hashOps.values();
        if (CollectionUtils.isEmpty(cartJsons)){
            return null;
        }
        return cartJsons.stream().map(cartJson -> JSON.parseObject(cartJson.toString(), Cart.class)).filter(Cart::getCheck).collect(Collectors.toList());
    }
}
