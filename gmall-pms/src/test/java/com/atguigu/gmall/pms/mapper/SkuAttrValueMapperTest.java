package com.atguigu.gmall.pms.mapper;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class SkuAttrValueMapperTest {

    @Autowired
    private SkuAttrValueMapper skuAttrValueMapper;

    @Test
    void queryMappingBySkuIds() {
        this.skuAttrValueMapper.queryMappingBySkuIds(Arrays.asList(38L, 39L, 40L, 41L)).forEach(System.out::println);
    }
}