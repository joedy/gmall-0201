package com.atguigu.gmall.item.vo;

import com.atguigu.gmall.pms.entity.CategoryEntity;
import com.atguigu.gmall.pms.entity.SkuImagesEntity;
import com.atguigu.gmall.pms.vo.ItemGroupVo;
import com.atguigu.gmall.pms.vo.SaleAttrValueVo;
import com.atguigu.gmall.sms.vo.ItemSaleVo;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Data
public class ItemVo {

    /// 面包屑
    // 三级分类：id name  V  O
    private List<CategoryEntity> categories;
    // 品牌信息         V  O
    private Long brandId;
    private String brandName;
    // spu信息        V   O
    private Long spuId;
    private String spuName;

    /// 基本信息
    // sku相关信息  V   O
    private Long skuId;
    private String title;
    private String subtitle;
    private BigDecimal price;
    private Integer weight;
    private String defaultImage;
    // sku图片列表      V   O
    private List<SkuImagesEntity> images;
    // 营销信息         V   O
    private List<ItemSaleVo> sales;
    // 是否有货         V   O
    private Boolean store = false;

    // spu下所有sku的销售属性       V   O
    // [{attrId: 3, attrName: 机身颜色, attrValues: ["暗夜黑", "白天白"]},
    // {attrId: 4, attrName: 运行内存, attrValues: ["6G", "8G"]},
    // {attrId: 5, attrName: 机身存储, attrValues: ["128G", "256G"]}]
    private List<SaleAttrValueVo> saleAttrs;

    // 当前sku的销售属性：{3: 暗夜黑, 4: 8G, 5: 256G}  V O
    private Map<Long, String> saleAttr;

    // 销售属性组合与skuId的映射关系：   V   O
    // {'暗夜黑,8G,256G': 101, '暗夜黑,12G,256': 102, '白天白,8G,256G': 103}
    private String skuJsons;

    // spu的图片列表     V   O
    private List<String> spuImages;

    // 规格参数分组
    private List<ItemGroupVo> groups;
}
