package com.atguigu.gmall.index.service;

import com.alibaba.fastjson.JSON;
import com.atguigu.gmall.common.bean.ResponseVo;
import com.atguigu.gmall.index.annotation.GmallCache;
import com.atguigu.gmall.index.feign.GmallPmsClient;
import com.atguigu.gmall.index.util.DistributedLock;
import com.atguigu.gmall.pms.entity.CategoryEntity;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RCountDownLatch;
import org.redisson.api.RLock;
import org.redisson.api.RReadWriteLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Service
public class IndexService {

    @Autowired
    private GmallPmsClient pmsClient;

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private DistributedLock distributedLock;

    @Autowired
    private RedissonClient redissonClient;

    private static final String KEY_PREFIX = "index:cates:";
    private static final String LOCK_PREFIX = "index:cates:lock:";

    public List<CategoryEntity> queryLvl1Categories() {
        ResponseVo<List<CategoryEntity>> responseVo = this.pmsClient.queryCategoriesByPid(0L);
        System.out.println("这也是目标方法。。。。。。。。。。。。。。。。。。。");
        return responseVo.getData();
    }

    @GmallCache(prefix = KEY_PREFIX, timeout = 129600, lock = LOCK_PREFIX, random = 14400)
    public List<CategoryEntity> queryLvl23CategoriesByPid(Long pid) {
        ResponseVo<List<CategoryEntity>> responseVo = this.pmsClient.queryLvl23CateogoriesByPid(pid);
        System.out.println("这是目标方法。。。。。。。。。");
        return responseVo.getData();
    }

    public List<CategoryEntity> queryLvl23CategoriesByPid2(Long pid) {
        // 1.先查询缓存，如果缓存可以命中则直接返回
        String json = this.redisTemplate.opsForValue().get(KEY_PREFIX + pid);
        if (StringUtils.isNotBlank(json)) {
            return JSON.parseArray(json, CategoryEntity.class);
        }

        // 添加分布式锁，防止缓存击穿
        RLock lock = this.redissonClient.getFairLock("index:cates:lock:" + pid);
        lock.lock();

        try {
            // 再次查询缓存，如果可以命中则直接返回（该请求获取锁的过程中，可能有其他请求已经把数据放入了缓存）
            String json2 = this.redisTemplate.opsForValue().get(KEY_PREFIX + pid);
            if (StringUtils.isNotBlank(json2)) {
                return JSON.parseArray(json2, CategoryEntity.class);
            }

            // 2.如果缓存无法命中，则查询数据库或者远程调用获取数据并放入缓存
            ResponseVo<List<CategoryEntity>> responseVo = this.pmsClient.queryLvl23CateogoriesByPid(pid);
            List<CategoryEntity> categoryEntities = responseVo.getData();
            // 放入缓存
            if (CollectionUtils.isEmpty(categoryEntities)) {
                // 为了防止缓存穿透，数据及时为空也缓存。
                this.redisTemplate.opsForValue().set(KEY_PREFIX + pid, JSON.toJSONString(categoryEntities), 5, TimeUnit.MINUTES);
            } else {
                // 为了防止缓存雪崩，给缓存时间添加随机值
                int random = new Random().nextInt(10);
                this.redisTemplate.opsForValue().set(KEY_PREFIX + pid, JSON.toJSONString(categoryEntities), 90 + random, TimeUnit.DAYS);
            }

            return categoryEntities;
        } finally {
            lock.unlock();
        }
    }

    public void testLock() {
        RLock lock = this.redissonClient.getFairLock("lock");
        lock.lock();
        try {
            String numString = this.redisTemplate.opsForValue().get("number");
            if (StringUtils.isBlank(numString)) {
                this.redisTemplate.opsForValue().set("number", "1");
            }
            int num = Integer.parseInt(numString);
            this.redisTemplate.opsForValue().set("number", String.valueOf(++num));
        } finally {
            lock.unlock();
        }
    }

    public void testLock3() {
        String uuid = UUID.randomUUID().toString();
        Boolean lock = this.distributedLock.tryLock("lock", uuid, 30);
        if (lock) {
            String numString = this.redisTemplate.opsForValue().get("number");
            if (StringUtils.isBlank(numString)) {
                this.redisTemplate.opsForValue().set("number", "1");
            }
            int num = Integer.parseInt(numString);
            this.redisTemplate.opsForValue().set("number", String.valueOf(++num));

            try {
                TimeUnit.SECONDS.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
//            this.testSubLock(uuid);

            this.distributedLock.unlock("lock", uuid);
        }
    }

    public void testLock2() {
        // 1.加锁：setnx lock 111
        String uuid = UUID.randomUUID().toString();
        Boolean lock = this.redisTemplate.opsForValue().setIfAbsent("lock", uuid, 3, TimeUnit.SECONDS);
        if (!lock) {
            // 3.重试
            try {
                Thread.sleep(40); // 1.减少争抢 2.防止栈内存溢出
                this.testLock();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        } else {
            // 为了防止死锁，给锁添加过期时间
            // this.redisTemplate.expire("lock", 3, TimeUnit.SECONDS);

            String numString = this.redisTemplate.opsForValue().get("number");
            if (StringUtils.isBlank(numString)) {
                this.redisTemplate.opsForValue().set("number", "1");
            }
            int num = Integer.parseInt(numString);
            this.redisTemplate.opsForValue().set("number", String.valueOf(++num));

            //this.testSubLock();

            // 2.解锁: del lock
            String script = "if redis.call('get', KEYS[1]) == ARGV[1] " +
                    "then " +
                    "   return redis.call('del', KEYS[1]) " +
                    "else " +
                    "   return 0 " +
                    "end";
            this.redisTemplate.execute(new DefaultRedisScript<>(script, Boolean.class), Arrays.asList("lock"), uuid);
//            if (StringUtils.equals(uuid, this.redisTemplate.opsForValue().get("lock"))){
//                this.redisTemplate.delete("lock");
//            }
        }
    }

    public void testSubLock(String uuid) {
        this.distributedLock.tryLock("lock", uuid, 30);
        System.out.println("==================");
        this.distributedLock.unlock("lock", uuid);
    }

    public void testRead() {
        RReadWriteLock rwLock = this.redissonClient.getReadWriteLock("rwLock");
        rwLock.readLock().lock(10, TimeUnit.SECONDS);

//        rwLock.readLock().unlock();
    }

    public void testWrite() {
        RReadWriteLock rwLock = this.redissonClient.getReadWriteLock("rwLock");
        rwLock.writeLock().lock(10, TimeUnit.SECONDS);
    }

    public void testLatch() {
        try {
            RCountDownLatch cdl = this.redissonClient.getCountDownLatch("cdl");
            cdl.trySetCount(6);
            cdl.await();

        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public void testCountDown() {
        RCountDownLatch cdl = this.redissonClient.getCountDownLatch("cdl");
        cdl.countDown();
    }
}
