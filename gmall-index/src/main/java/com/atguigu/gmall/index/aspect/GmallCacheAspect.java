package com.atguigu.gmall.index.aspect;

import com.alibaba.fastjson.JSON;
import com.atguigu.gmall.index.annotation.GmallCache;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.redisson.api.RBloomFilter;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Random;
import java.util.concurrent.TimeUnit;

@Component
@Aspect // 声明该类是一个切面类
public class GmallCacheAspect {

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private RedissonClient redissonClient;

    @Autowired
    private RBloomFilter bloomFilter;

    @Pointcut("@annotation(com.atguigu.gmall.index.annotation.GmallCache)")
    public void pointcut(){
    }

    @Around("pointcut()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {

        // 获取方法签名
        MethodSignature signature = (MethodSignature)joinPoint.getSignature();
        // 获取方法对象
        Method method = signature.getMethod();

        // 获取目标方法上的注解对象：GmallCache
        GmallCache gmallCache = method.getAnnotation(GmallCache.class);
        // 获取缓存前缀
        String prefix = gmallCache.prefix();
        // 目标方法的形参
        Object[] args = joinPoint.getArgs();
        // 把目标方法形参数组转化成以逗号分割的字符串
        String argString = StringUtils.join(args, ",");
        // 组装缓存的key
        String key = prefix + argString;
        // 1.先查询缓存，如果缓存可以命中则直接返回
        String json = this.redisTemplate.opsForValue().get(key);
        if (StringUtils.isNotBlank(json)){
            // 把缓存的json字符串反序列化为方法返回结果集类型
            return JSON.parseObject(json, method.getReturnType());
        }

        // 为了防止缓存穿透，使用布隆过滤器。
        if (!bloomFilter.contains(key)){
            return null;
        }

        // 2.为了防止缓存击穿，添加了分布式锁
        RLock lock = this.redissonClient.getLock(gmallCache.lock() + argString);
        lock.lock();

        try {
            // 3.当前线程获取分布式锁的过程中，可能有其他线程已经获取了分布式锁，此时再次查询缓存，如果可以命中，直接返回
            String json2 = this.redisTemplate.opsForValue().get(key);
            if (StringUtils.isNotBlank(json2)){
                // 把缓存的json字符串反序列化为方法返回结果集类型
                return JSON.parseObject(json2, method.getReturnType());
            }

            // 4.执行目标方法
            Object result = joinPoint.proceed(args);

            // 5.把目标方法的返回结果集放入缓存，并释放分布式锁
            // 为了防止缓存雪崩给缓存时间添加随机值
            int timeout = gmallCache.timeout() + new Random().nextInt(gmallCache.random());
            this.redisTemplate.opsForValue().set(key, JSON.toJSONString(result), timeout, TimeUnit.MINUTES);

            return result;
        } finally {
            lock.unlock();
        }
    }

    //@Before("pointcut()")
    public void before(){
        System.out.println("这是前置通知。。。。。。。。。。。。。。。。。。");
    }

}
