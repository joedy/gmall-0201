package com.atguigu.gmall.index.annotation;

import java.lang.annotation.*;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface GmallCache {

    /**
     * 指定缓存前缀，默认值：gmall:cache:
     * @return
     */
    String prefix() default "gmall:cache:";

    /**
     * 指定缓存的过期时间，默认值：1440min
     * @return
     */
    int timeout() default 1440;

    /**
     * 为了防止缓存击穿，添加了分布式锁，这里可以指定分布式锁前缀，默认：gmall:lock:
     * @return
     */
    String lock() default "gmall:lock:";

    /**
     * 为了防止缓存雪崩，给缓存时间添加随机值，这里可以指定随机值的返回，默认：1440min
     * @return
     */
    int random() default 1440;
}
