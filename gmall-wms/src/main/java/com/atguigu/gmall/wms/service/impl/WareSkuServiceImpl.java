package com.atguigu.gmall.wms.service.impl;

import com.alibaba.fastjson.JSON;
import com.atguigu.gmall.common.exception.OrderException;
import com.atguigu.gmall.wms.vo.SkuLockVo;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atguigu.gmall.common.bean.PageResultVo;
import com.atguigu.gmall.common.bean.PageParamVo;

import com.atguigu.gmall.wms.mapper.WareSkuMapper;
import com.atguigu.gmall.wms.entity.WareSkuEntity;
import com.atguigu.gmall.wms.service.WareSkuService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;


@Service("wareSkuService")
public class WareSkuServiceImpl extends ServiceImpl<WareSkuMapper, WareSkuEntity> implements WareSkuService {

    @Autowired
    private RedissonClient redissonClient;

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private WareSkuMapper wareSkuMapper;

    private static final String KEY_PREFIX = "stock:lock:";
    private static final String STOCK_PREFIX = "stock:info:";

    @Override
    public PageResultVo queryPage(PageParamVo paramVo) {
        IPage<WareSkuEntity> page = this.page(
                paramVo.getPage(),
                new QueryWrapper<WareSkuEntity>()
        );

        return new PageResultVo(page);
    }

    @Transactional
    @Override
    public List<SkuLockVo> checkLock(List<SkuLockVo> lockVos, String orderToken) {

        // 判断列表是否为空，如果为空则直接抛出异常
        if (CollectionUtils.isEmpty(lockVos)){
            throw new OrderException("你没有选择要购买的商品！");
        }

        // 遍历所有商品验库存并锁库存
        lockVos.forEach(lockVo -> {
            this.checkAndLock(lockVo);
        });

        // 判断是否存在锁定失败的库存，如果有则解锁所有锁定成功了的库存
        if (lockVos.stream().anyMatch(lockVo -> !lockVo.getLock())){
            // 获取锁定成功的库存，并解锁
            lockVos.stream().filter(SkuLockVo::getLock).forEach(lockVo -> {
                this.wareSkuMapper.unlock(lockVo.getWareSkuId(), lockVo.getCount());
            });
            // 返回锁定信息
            return lockVos;
        }

        // 如果锁定成功，需要缓存锁定信息到redis，以便于将来减库存或者解锁库存（orderToken: List<skuLockVo>的json字符串）
        this.redisTemplate.opsForValue().set(STOCK_PREFIX + orderToken, JSON.toJSONString(lockVos));

        // 为了避免order服务器宕机带来的库存被锁死的情况发生，发送延时消息定时解锁库存
        this.rabbitTemplate.convertAndSend("ORDER.EXCHANGE", "stock.ttl", orderToken);

        // 如果锁定成功，返回null
        return null;
    }

    /**
     * 对每一个购买的商品验库存并锁库存
     * 并且要使用分布式锁保证原子性
     * @param lockVo
     */
    private void checkAndLock(SkuLockVo lockVo){
        RLock lock = this.redissonClient.getLock(KEY_PREFIX + lockVo.getSkuId());
        lock.lock();

        try {
            // 验库存
            List<WareSkuEntity> wareSkuEntities = this.wareSkuMapper.check(lockVo.getSkuId(), lockVo.getCount());
            if (CollectionUtils.isEmpty(wareSkuEntities)){
                lockVo.setLock(false);
                return;
            }

            // 锁库存：根据大数据接口分析成本最低的仓库发货
            WareSkuEntity wareSkuEntity = wareSkuEntities.get(0);
            if (this.wareSkuMapper.lock(wareSkuEntity.getId(), lockVo.getCount()) == 1) {
                lockVo.setLock(true);
                lockVo.setWareSkuId(wareSkuEntity.getId());
            } else {
                lockVo.setLock(false);
            }

        } finally {
            lock.unlock();
        }
    }

}